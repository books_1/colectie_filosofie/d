# D

## Content

```
./D. M. Pippidi:
D. M. Pippidi - Fragmentele eleatilor.pdf

./Damascius:
Damascius - Despre primele principii.pdf

./Daniel Barbu:
Daniel Barbu - Bizant contra Bizant.pdf

./Daniel Haussman:
Daniel Haussman - Filozofia stiintei economice (Antologie).pdf

./Daniel Klein & Thomas Cathcart:
Daniel Klein & Thomas Cathcart - Aristotel si furnicarul merg la Washington (Nemira).pdf

./Dave Robinson & Judy Groves:
Dave Robinson & Judy Groves - Cate ceva despre filozofie.pdf

./David Bohm:
David Bohm - Plenitudinea lumii si ordinea ei.pdf

./David Hume:
David Hume - Cercetare asupra intelectului omenesc.pdf
David Hume - Eseuri Politice.pdf

./David Le Breton:
David Le Breton - Antropologia corpului si modernitatea.pdf

./David Lewis:
David Lewis - Despre pluralitatea lumilor.pdf

./David Ross:
David Ross - Aristotel.pdf

./David Schmidtz:
David Schmidtz - Elemente ale dreptatii.pdf

./Dennis Collin:
Dennis Collin - Marile notiuni filosofice 5, munca si tehnica.pdf

./Desmond Morris:
Desmond Morris - Zoomenirea.pdf

./Dictionar:
Dictionar enciclopedic de filosofie.pdf

./Dimitrie Cantemir:
Dimitrie Cantemir - Mic compendiu.pdf

./Diogenes din Oinoanda & Epicur:
Diogenes din Oinoanda & Epicur - Epicurea.pdf

./Diogenes Laertios:
Diogenes Laertios - Despre vietile si doctrinele filosofilor.pdf

./Dragos Popescu:
Dragos Popescu - Logica speculativa hegeliana.pdf

./Dumitru Craciun:
Dumitru Craciun - Logica si teoria argumentarii.pdf

./Dumitru D. Rosca:
Dumitru D. Rosca - Existenta tragica.pdf
Dumitru D. Rosca - Linii si figuri.pdf

./Dumitru Gheorghiu:
Dumitru Gheorghiu - Existenta, contradictie si adevar.pdf
Dumitru Gheorghiu - Logica generala, vol. 1.pdf
Dumitru Gheorghiu - Logica generala, vol. 2.pdf

./Dumitru Staniloae:
Dumitru Staniloae - Pozitia domnului Lucian Blaga fata de crestinism si ortodoxie.pdf

./Duns Scotus:
Duns Scotus - Despre primul principiu.pdf
```

